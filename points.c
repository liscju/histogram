#include "points.h"

static int int_cmp(const void* a, const void *b) {
	const int *ia = (const int *)a;
	const int *ib = (const int *)b;
	
	return *ia - *ib;
}

int *create_random_points(int count, int min, int max) {
	int i;
	int *points = calloc(count, sizeof(int));
	int range = max - min;	

	for (i=0;i < count;i++) {
		points[i] = (rand() % range) + min;
	}	

	return points;
}

void print_points(int *points, int points_len) {
	int i;
	printf("points = {");

	qsort(points, points_len, sizeof(int), int_cmp);

	for (i=0;i < points_len;i++) {
		printf("%d, ", points[i]);
	}
	printf("}\n");
}


