#include "histogram.h"

#include <omp.h>

int calculate_point_interval_index(
	int point,
	int min,
	int max,
	int interval_count) {
	
	int interval_step = (max - min) / interval_count;

	int calculate_interval = (point - min) / interval_step;

	int normalized_interval;

	if (calculate_interval < interval_count) {
		normalized_interval = calculate_interval;
	} else {
		normalized_interval = interval_count - 1;
	}

	return normalized_interval;	
}

int *calculate_histogram_sequential(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count) {

	int i;	
	int *intervals = calloc(interval_count, sizeof(int));	

	for (i=0;i < points_len;i++) {
		int curr_point_interval_index = 
			calculate_point_interval_index(
					points[i], min, max, interval_count);

		intervals[curr_point_interval_index]++;
	}
	
	return intervals;
}

static omp_lock_t* create_locks(int count) {
	int i;
	omp_lock_t *locks = calloc(count, sizeof(omp_lock_t));	
	for (i=0;i < count;i++) {
		omp_init_lock(&locks[i]);
	}
	return locks;
}

static void destroy_locks(omp_lock_t *locks, int count) {
	int i;
	for (i=0;i < count;i++) {
		omp_destroy_lock(&locks[i]);
	}
}

int *calculate_histogram_parallel(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count) {

	int i;	
	int *intervals = calloc(interval_count, sizeof(int));	
	omp_lock_t *indexlocks = create_locks(interval_count);

#pragma omp parallel for private(i)
	for (i=0;i < points_len;i++) {
		int curr_point_interval_index = 
			calculate_point_interval_index(
					points[i], min, max, interval_count);

		omp_set_lock(&indexlocks[curr_point_interval_index]);

			intervals[curr_point_interval_index]++;

		omp_unset_lock(&indexlocks[curr_point_interval_index]);
	}

	destroy_locks(indexlocks, interval_count);
	
	return intervals;
}

int *calculate_histogram_parallel_reduction(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count) {

	int *intervals = calloc(interval_count, sizeof(int));	

#pragma omp parallel
	{
		int i, j, *intervals_private;	
		intervals_private = calloc(interval_count, sizeof(int));

#pragma omp for nowait		
		for (i=0;i < points_len;i++) {
			int curr_point_interval_index = 
				calculate_point_interval_index(
					points[i], min, max, interval_count);

			intervals_private[curr_point_interval_index]++;
		}

#pragma omp critical
		{
			for (i=0;i < interval_count;i++) {
				intervals[i] += intervals_private[i];
			}
		}
	}
	
	return intervals;
}

void show_histogram(
		int *histogram,
		int min,
		int max,
		int interval_count) {
	
	int i;
	int interval_step = (max - min) / interval_count;
	int current_interval_begin = min;

	for (i=0;i<interval_count-1;i++) {
		printf("<%d;%d) - %d\n",
			current_interval_begin,
			current_interval_begin + interval_step,
			histogram[i]);
		current_interval_begin += interval_step;
	}

	printf("<%d;%d) - %d\n",
			current_interval_begin,
			max,
			histogram[i]);
}

