#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#include "points.h"
#include "histogram.h"

void usage(char *prog_name) {
	printf("Usage:\n%s METHOD POINTS_COUNT RANGE_MIN RANGE_MAX INTERVAL_COUNT\n",
	       prog_name);
	printf("Method is: 0(for sequential) 1(for parallel) 2(for parallel reduction)\n");
	exit(-1);
}

int main(int argc, char **argv) {
	srand(time(NULL));

	if (argc != 6) {
		usage(argv[0]);
	}

	int i;
	
	int method = atoi(argv[1]);
	int points_count = atoi(argv[2]);
	int min = atoi(argv[3]);
	int max = atoi(argv[4]);
	int interval_count = atoi(argv[5]);

	int *points = create_random_points(points_count, min, max);
	int *histogram;

	struct timeval timeStart, timeEnd;

	gettimeofday(&timeStart, NULL);

	if (method == 0) {
		printf("Run sequential method for %d points\n"
                       "%d min, %d max, %d interval_count\n",
				points_count,
				min,
				max,
				interval_count);

		histogram = calculate_histogram_sequential(
				points, 
				points_count,
				min,
				max,
				interval_count);

	} else if (method == 1) {	
		printf("Run parallel method for %d points\n"
                       "%d min, %d max, %d interval_count\n"
		       "with %d threads\n",
				points_count,
				min,
				max,
				interval_count,
				omp_get_max_threads());

		histogram = calculate_histogram_parallel(
				points, 
				points_count,
				min,
				max,
				interval_count);

	} else if (method == 2) {	
		printf("Run parallel reduction method for %d points\n"
                       "%d min, %d max, %d interval_count\n"
		       "with %d threads\n",
				points_count,
				min,
				max,
				interval_count,
				omp_get_max_threads());

		histogram = calculate_histogram_parallel_reduction(
				points, 
				points_count,
				min,
				max,
				interval_count);

	} else {
		printf("Error: Unrecognized method %d\n", method);
		exit(-1);
	}

	/**
	print_points(points, points_count);

	show_histogram(
		histogram,
		min,
		max,
		interval_count);
	*/

	gettimeofday(&timeEnd, NULL);
	
	int time = (timeEnd.tv_sec - timeStart.tv_sec) * 1000 +
	           (timeEnd.tv_usec - timeStart.tv_usec) / 1000;

	printf("Czas %d ms \n", time);

	return 0;
}
