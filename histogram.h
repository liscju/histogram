#ifndef __HISTOGRAM_H__
#define __HISTOGRAM_H__

#include <stdio.h>
#include <stdlib.h>

int *calculate_histogram_sequential(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count);

int *calculate_histogram_parallel(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count);

int *calculate_histogram_parallel_reduction(
	int *points, 
	int points_len,
	int min,
	int max,
	int interval_count);

void show_histogram(
		int *histogram,
		int min,
		int max,
		int interval_count);

#endif //__HISTOGRAM_H__
