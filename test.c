#include <assert.h>
#include <stdlib.h>
#include <omp.h>

#include "points.h"
#include "histogram.h"

int assert_equals(
	int interval_count,
	int *histogram_sequential, 
	int *histogram_parallel) {
	int i;
	int equals = 1;

	for (i = 0;i < interval_count; i++) {
		equals &= (histogram_sequential[i] == histogram_parallel[i]);
	}

	return equals;
}

void print_test_failed(
	int *points,
	int points_len,
	int *histogram_seq,
	int *histogram_par,
	int min,
	int max,
	int interval_count) {

	printf("FAILED\n");
	printf("Info:\n\n");

	print_points(points, points_len);
	
	printf("Histogram for sequential:\n");
	show_histogram(
		histogram_seq,
		min,
		max,
		interval_count);

	printf("Histogram for parallel:\n");
	show_histogram(
		histogram_par,
		min,
		max,
		interval_count);

	exit(-1);
}

void run_test(int num_threads,
	      int points_count, 
	      int min,
	      int max, 
	      int interval_count) {

	printf("Running test for procs = %d points_count = %d, min = %d, "
	       "max = %d, interval_count = %d: ", 
	       num_threads, points_count, min, max, interval_count);

	int *points = 
		create_random_points(
			points_count,
			min, max);

	int *histogram_sequential = 
		calculate_histogram_sequential(
				points, 
				points_count,
				min,
				max,
				interval_count);

	int *histogram_parallel = 
		calculate_histogram_parallel(
				points, 
				points_count,
				min,
				max,
				interval_count);

	int *histogram_parallel_reduction = 
		calculate_histogram_parallel_reduction(
				points, 
				points_count,
				min,
				max,
				interval_count);

	if (!assert_equals(interval_count, 
		histogram_sequential, histogram_parallel)) {
		
		print_test_failed(
			points,
			points_count,
			histogram_sequential,
			histogram_parallel,
			min,
			max,
			interval_count);	
	}

	// histogram_sequential == histogram_parallel	

	if (!assert_equals(interval_count, 
		histogram_parallel,histogram_parallel_reduction)) {
		
		print_test_failed(
			points,
			points_count,
			histogram_parallel,
			histogram_parallel_reduction,
			min,
			max,
			interval_count);	
	}

	printf("PASSED\n");
}

int main() {
	srand(time(NULL));

	int i;

	int test_cases[][5] = {
		// {num_threads, points_count, min, max, interval_count}
		{1, 50, 0, 10, 5},
		{1,  200, 0, 100, 40},
		{4, 100, 0, 30, 5},
		{4,  500, 0, 300, 8},
		{8, 200, 0, 40, 10},
		{8,  1000, 0, 400, 10},
		{12, 200, 0, 100, 4},
		{16, 200, 0, 200, 15},
		{20, 200, 0, 200, 15},
	};

	int test_cases_len = sizeof(test_cases) / (5 * sizeof(int)); 	

	for (i = 0;i < test_cases_len;i++) {		
		int omp_num_threads = test_cases[i][0];
		omp_set_num_threads(omp_num_threads);

		int points_count = test_cases[i][1];
		int min = test_cases[i][2];
		int max = test_cases[i][3];
		int interval_count = test_cases[i][4];

		run_test(
			omp_get_max_threads(),
			points_count, 
			min, 
			max, 
			interval_count);
	}

	printf("All tests passed succesfully!!\n");
	return 0;
}
