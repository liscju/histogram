all: main.o

clean:
	rm -f *.o

main.o: main.c points.o histogram.o
	gcc -fopenmp main.c -o main.o points.o histogram.o

points.o: points.h points.c
	gcc -fopenmp points.c -c

histogram.o: histogram.h histogram.c
	gcc -fopenmp histogram.c -c 

test: test.c points.o histogram.o
	gcc -fopenmp test.c -o test.o points.o histogram.o
	./test.o
