#ifndef __POINTS_H__
#define __POINTS_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int *create_random_points(int count, int min, int max);
void print_points(int *points, int points_len);

#endif /** __POINTS_H__ */
